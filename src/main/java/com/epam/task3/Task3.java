package com.epam.task3;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task3 {
    private Random random = new Random();

    public List<Integer> listIterator(final int count) {
        List<Integer> result = Stream
                .iterate(random.nextInt(10000), n -> random.nextInt(10000))
                .limit(count)
                .collect(Collectors.toList());
        return result;
    }

    public List<Integer> listGenerator(final int count) {
        List<Integer> result = Stream
                .generate(() -> random.nextInt(10000))
                .limit(count)
                .collect(Collectors.toList());
        return result;
    }

    public List<Integer> streamOf() {
        return Stream
                .of(random.nextInt(10000), random.nextInt(10000),
                        random.nextInt(10000))
                .collect(Collectors.toList());
    }

    public List<Integer> streamBuider() {
        return Stream.<Integer>builder()
                .add(random.nextInt(10000))
                .add(random.nextInt(10000))
                .build()
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        Task3 task3 = new Task3();
        List<Integer> generated = task3.listGenerator(4);
        System.out.println(generated);
        Stream<Integer> generatedStream = generated
                .stream();
        int sum = generatedStream.reduce(Integer::sum).get();
        System.out.println("Sum: " + sum);

        List<Integer> streamOf = task3.streamOf();
        System.out.println(streamOf);
        Stream<Integer> streamOfStream = streamOf.stream();
        System.out.println(streamOfStream.reduce((a, b) -> a + b));

        List<Integer> iter = task3.listIterator(5);
        System.out.println(iter);
        Stream<Integer> integerStream = iter.stream();
        System.out.println(integerStream.max((n1, n2) -> n1 - n2));


    }
}