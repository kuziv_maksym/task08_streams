package com.epam.task4;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Model {
    private List<String> listOfUniq;
    private List<String> listSort;

    public void initialize(ArrayList<String> list) {
        listOfUniq = listUniq(list);
        listSort = sortedList((ArrayList<String>) listOfUniq);
        show();
    }

    public List<String> listUniq(ArrayList<String> list) {
        return list.stream()
                .flatMap(s -> Stream.of(s.split(" ")))
                .peek((s)->{})
                .distinct()
                .collect(Collectors.toList());
    }

    public List<String> sortedList(ArrayList<String> line) {
        return line.stream()
                .sorted()
                .collect(Collectors.toList());
    }
    void show() {
        System.out.println("list uniq = " + listOfUniq);
        System.out.println("list sort = " + listSort);
    }
}
