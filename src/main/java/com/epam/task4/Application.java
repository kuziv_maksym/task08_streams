package com.epam.task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class Application {
    public static void main(String[] args) {

        Controller controller = new Controller(new Model());
        controller.task4();

    }
}
