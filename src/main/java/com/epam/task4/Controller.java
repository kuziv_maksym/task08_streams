package com.epam.task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Controller {
    public Model model;
    private List<String> text;
    private Scanner scanner;
    private String line;
    private static Logger log = LogManager.getLogger();
    public Controller(Model model) {
        this.model = new Model();
        scanner = new Scanner(System.in);
    }
    public void task4() {
        text = new ArrayList<>();
        System.out.println("Input text(End is empty line");
        line = scanner.nextLine();
        try {
            while (!line.equals("")) {
                text.add(line);
                line = scanner.nextLine();
            }
        } catch (Exception e) {
            log.error("Except input row");
            throw new RuntimeException("Except input row");
        }

        model.initialize((ArrayList<String>) text);

    }

}
