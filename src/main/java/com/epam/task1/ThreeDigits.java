package com.epam.task1;


@FunctionalInterface
public interface ThreeDigits {
    int add(int a, int b, int c);
}
