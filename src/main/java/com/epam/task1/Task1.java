package com.epam.task1;

public class Task1 {
    public static void main(String[] args) {
        ThreeDigits threeDigits = ((a, b, c) -> Math.max(Math.max(a,b), c));
        System.out.println(threeDigits.add(31,22,11));

        ThreeDigits threeDigits1 = ((a,b,c) -> a + b + c);
        System.out.println(threeDigits1.add(44,55,121));

    }
}
